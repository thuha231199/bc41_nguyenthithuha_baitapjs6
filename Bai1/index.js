/**
 * Bài 1: Tìm số nguyên dương nhỏ nhất sao cho: 1 + 2 + ... n > 1000
 * progrees: var sum = 0; count= 1; Tạo ra một vòng lặp sum < 10000 ;  count++;
 * sum += count
 * 
 */

document.getElementById('btn-1').onclick = function() {
    var sum = 0;
    var count = 1;
    for(; sum < 10000;count++) {
        sum += count;
        document.getElementById('resut').innerHTML = `Số nguyên dương nhỏ nhất với tổng từ 1 --> nó > 10000 là : ${count}`
    }
    console.log(sum)
}


/**
 * Bài 2: input: value x,y
 *  progress: cho sum = 0
 * dùng for loop i = 1, i <=n, i++
 * dngf hàm math.pow(x,i)
 * sum+= hàm math.pow(x,i)
 * 
 * output: in ra sum
 * 
 */

document.getElementById('btn__bai2').onclick = function() {
    var x = document.getElementById('input__bai2-x').value*1;
    var n = document.getElementById('input__bai2-n').value*1;
    var sum = 0;
    for(var i = 1; i <= n; i++) {
        sum += Math.pow(x,i);
    }
    document.getElementById('result__bai2').innerHTML = `Kết quả: ${sum}`
}


/**
 * Bài 3: Tính Giai Thừa
 * input: người dùng nhập vào n
 * progress: gan n cho x
 * for loop i = n-1, điểu kiện i<= 1; i--
 * x*= i
 * ouput: in ra i
 */
document.getElementById('btn-3').onclick = function() {
    var tinhGiaiThua = document.getElementById('input_3').value *1;
    var ketQua = tinhGiaiThua
    for (var i = tinhGiaiThua -1; i >= 1; i-- ) {
        ketQua *= i;
    }
    document.getElementById('result_3').innerHTML = `Giai thừa của ${tinhGiaiThua} = ${ketQua}`
}

/**
 * Bài 4
 */
document.getElementById('btn-4').onclick = function() {
var div = document.querySelectorAll('.Div')
    console.log(div);
    for(var i = 0; i<div.length;i++) {
        if(i %2 ==0) {
            div[i].style.background = 'blue';
        } else  {
            div[i].style.background = 'red';
        }
    }
}